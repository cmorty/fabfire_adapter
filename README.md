FabFire Adapter
===
The FabFire adapter translates mqtt messages from the reader hardware to api calls to bffhd.

# Usage
## Configuration in `config.toml`
1. Set `hostname` and `port` for your MQTT Broker and bffhd instance
2. Add your readers to the `[readers]` section. Every reader needs a unique subsection name:
```toml
[readers]
    [readers.REPLACEME]
    id = "111"
    machine = "urn:fabaccess:resource:Testmachine"

    [readers.REPLACEMETOO]
    id = "222"
    machine = "urn:fabaccess:resource:Another"
```

## Running
1. Install the requirements with `pip install -r requirements.txt`
2. Run the adapter with at least python 3.7
```shell
python3 main.py
```
3. The adapter has to remain running for the readers to work

# Provisioning

See the [provisioning tool](https://gitlab.com/fabinfra/fabaccess/FabFire-Provisioning-Tool) repo.