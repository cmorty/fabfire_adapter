from dataclasses import dataclass
from typing import Optional


@dataclass(frozen=True)
class Host:
    hostname: str
    port: int
    username: Optional[str] = None
    password: Optional[str] = None